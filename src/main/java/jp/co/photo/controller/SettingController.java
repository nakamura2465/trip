package jp.co.photo.controller;

import javax.servlet.ServletContext;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import jp.co.photo.dto.UserDto;
import jp.co.photo.form.UserForm;
import jp.co.photo.service.UserService;

@Controller
@SessionAttributes({"loginUser"})
public class SettingController {

	@Autowired
	private UserService userService;

	@Autowired
	ServletContext context;

	@RequestMapping(value = "/setting", method = RequestMethod.GET)
	public String setting(@ModelAttribute("loginUser") UserDto loginUser,Model model) {
		UserForm userForm = new UserForm();
		userForm.setPassword("");
		BeanUtils.copyProperties(loginUser, userForm);
		model.addAttribute("userForm",userForm);
		return "setting";
	}
	@RequestMapping(value = "/setting", method = RequestMethod.POST)
	public String setting(UserForm userForm,Model model) {
			UserDto loginUser = userService.settingUserBySettingForm(userForm);
			model.addAttribute("loginUser",loginUser);
	        return "setting";
	}
}
