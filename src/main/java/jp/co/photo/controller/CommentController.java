package jp.co.photo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.photo.form.CommentForm;
import jp.co.photo.service.CommentService;

@Controller
public class CommentController {

	@Autowired
	private CommentService commentService;

	@RequestMapping(value = "/comment", method = RequestMethod.POST)
	public String Comment(@ModelAttribute CommentForm commentForm, Model model) {
		commentService.insert(commentForm);
		return "redirect:/";
	}
}
