package jp.co.photo.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import jp.co.photo.dto.UserDto;
import jp.co.photo.form.UserForm;
import jp.co.photo.service.UserService;


@Controller
@SessionAttributes(value= {"loginUser"})
public class LoginController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(Model model) {
		UserForm userForm = new UserForm();
		model.addAttribute("userForm",userForm);
		return "login";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(UserForm userForm,Model model ) {
		UserDto loginUser = userService.selectUserByLoginForm(userForm);
		model.addAttribute("loginUser",loginUser);
		return "redirect:./";
	}
}