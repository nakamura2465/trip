package jp.co.photo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.photo.form.UserForm;
import jp.co.photo.service.UserService;

@Controller
public class SignUpController {

	@Autowired private UserService userService;

	@RequestMapping(value="/signup" , method = RequestMethod.GET)
	public String signUp(Model model) {
		UserForm userForm = new UserForm();
		model.addAttribute("userForm",userForm);
		return "signup";
	}

	@RequestMapping(value="/signup" , method = RequestMethod.POST)
	public String signUp(@ModelAttribute UserForm userForm,Model model ) {
		userService.insertUserBySignUp(userForm);
		return "redirect:./";
	}
}