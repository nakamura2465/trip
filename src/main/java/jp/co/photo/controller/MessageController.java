package jp.co.photo.controller;

import javax.servlet.ServletContext;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.photo.form.MessageForm;
import jp.co.photo.service.MessageService;


@MultipartConfig(location="/tmp", maxFileSize=1048576)
@Controller
public class MessageController {

	@Autowired
	private MessageService messageService;

	@RequestMapping(value = "/message/new", method = RequestMethod.GET)
	public String Message(Model model) {
		MessageForm messageForm = new MessageForm();
		model.addAttribute("messageForm", messageForm);
		return "message";
	}

	@Autowired
	HttpServletRequest request;
	ServletContext servletcontext;

	@RequestMapping(value = "/message/new", method = RequestMethod.POST)
	public String Message(@ModelAttribute MessageForm messageForm) {
		Integer index = messageService.insert(messageForm);
		/*Integer name[] = new Integer[4];
		try {
			Part part0;
			part0 = request.getPart("messagePicture0");
			Part part1 = request.getPart("messagePicture1");
			Part part2 = request.getPart("messagePicture2");
			Part part3 = request.getPart("messagePicture3");
			if(part0 != null || part1 != null || part2 != null || part3 != null) {
				if(part0 != null) {
					name[0] = index;

					if(part1 != null) {
						name[1] = name[0]++;

						if(part2 != null) {
							name[2] = name[1]++;

							if(part3 != null) {
								name[3] = name[2]++;

							}
						}else {
							name[3] = name[1]++;
						}
					}else {
						if(part2 != null) {
							name[2] = name[0]++;

							if(part3 != null) {
								name[3] = name[2]++;

							}

						}else {
							name[3] = name[0]++;
						}
					}
				}else {
					if(part1 != null) {
						name[1] = index;

						if(part2 != null) {
							name[2] = name[1]++;

							if(part3 != null) {
								name[3] = name[2]++;

							}
						}else {
							name[3] = name[1]++;
						}
					}else {
						if(part2 != null) {
							name[2] = index;

							if(part3 != null) {
								name[3] = name[2]++;

							}

						}else {
							if(part3 != null) {
								name[3] = index;
							}
						}
					}
				}
				if(name[0] != null) {
					part0.write(servletcontext.getRealPath("/WEB-INF/uploaded/messages") + "/" + String.valueOf(name[0]));
				}
				if(name[1] != null) {
					part1.write(servletcontext.getRealPath("/WEB-INF/uploaded/messages") + "/" + String.valueOf(name[1]));
				}
				if(name[2] != null) {
					part2.write(servletcontext.getRealPath("/WEB-INF/uploaded/messages") + "/" + String.valueOf(name[2]));
				}
				if(name[3] != null) {
					part3.write(servletcontext.getRealPath("/WEB-INF/uploaded/messages") + "/" + String.valueOf(name[3]));
				}

				messageService.insertPicture(name, index);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ServletException e) {
			e.printStackTrace();
		}*/
		return "redirect:../";
	}

}
