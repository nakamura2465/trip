package jp.co.photo.form;

import java.io.File;
import java.util.Date;

public class MessageForm {

	private Integer id;
	private Integer userId;
	private String title;
	private Integer categoryId;
	private String content;
	private File picture0;
	private File picture1;
	private File picture2;
	private File picture3;
	private Date createdDate;
	private Date updatedDate;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = Integer.parseInt(userId);
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = Integer.parseInt(categoryId);
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}

	public File getPicture0() {
		return picture0;
	}
	public void setPicture0(File picture0) {
		this.picture0 = picture0;
	}
	public File getPicture1() {
		return picture1;
	}
	public void setPicture1(File picture1) {
		this.picture1 = picture1;
	}
	public File getPicture2() {
		return picture2;
	}
	public void setPicture2(File picture2) {
		this.picture2 = picture2;
	}
	public File getPicture3() {
		return picture3;
	}
	public void setPicture3(File picture3) {
		this.picture3 = picture3;
	}

	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}
