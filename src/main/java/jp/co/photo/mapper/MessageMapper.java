package jp.co.photo.mapper;

import jp.co.photo.form.MessageForm;

public interface MessageMapper {
	int insert(MessageForm messageForm);
	int selectId();
}
