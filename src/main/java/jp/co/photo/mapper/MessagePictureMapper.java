package jp.co.photo.mapper;

import java.util.List;

import jp.co.photo.entity.MessagePictureEntity;

public interface MessagePictureMapper {
	int insert(List<MessagePictureEntity> messagePictureEntities);

}
