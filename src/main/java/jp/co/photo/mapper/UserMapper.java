package jp.co.photo.mapper;

import jp.co.photo.entity.UserEntity;
import jp.co.photo.form.UserForm;

public interface UserMapper {
	UserEntity selectUserByLoginForm(UserForm userForm);
	int insertUserBySignUp(UserForm userForm);
	int updateUserBySettingForm(UserForm userForm);
}
