package jp.co.photo.mapper;

import jp.co.photo.form.CommentForm;

public interface CommentMapper {

	int insert(CommentForm commentForm);
}
