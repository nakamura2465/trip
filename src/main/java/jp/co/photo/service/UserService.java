package jp.co.photo.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.photo.dto.UserDto;
import jp.co.photo.entity.UserEntity;
import jp.co.photo.form.UserForm;
import jp.co.photo.mapper.UserMapper;
import jp.co.photo.utils.CipherUtil;

@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;

    public UserDto selectUserByLoginForm(UserForm userForm) {
    	getEncryptPassword(userForm);
    	UserEntity userEntity = userMapper.selectUserByLoginForm(userForm);
    	UserDto userDto = new UserDto();
    	BeanUtils.copyProperties(userEntity, userDto);
    	return userDto;
    }
    public void insertUserBySignUp(UserForm userForm) {
    	getEncryptPassword(userForm);
    	userMapper.insertUserBySignUp(userForm);
    }
    public UserDto settingUserBySettingForm(UserForm userForm) {
    	getEncryptPassword(userForm);
    	userMapper.updateUserBySettingForm(userForm);
    	UserEntity userEntity = userMapper.selectUserByLoginForm(userForm);
    	UserDto userDto = new UserDto();
    	BeanUtils.copyProperties(userEntity, userDto);
    	return userDto;
    }
    private void getEncryptPassword(UserForm userForm) {
    	String encPassword = CipherUtil.encrypt(userForm.getPassword());
    	userForm.setPassword(encPassword);
    }

}
