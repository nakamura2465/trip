package jp.co.photo.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.photo.form.CommentForm;
import jp.co.photo.mapper.CommentMapper;

@Service
public class CommentService {

	@Autowired
	private CommentMapper commentMapper;

	public void insert(CommentForm commentForm) {
		int index = commentMapper.insert(commentForm);
	}
}
