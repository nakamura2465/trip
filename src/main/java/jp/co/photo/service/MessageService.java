package jp.co.photo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.photo.entity.MessagePictureEntity;
import jp.co.photo.form.MessageForm;
import jp.co.photo.mapper.MessageMapper;
import jp.co.photo.mapper.MessagePictureMapper;

@Service
public class MessageService {

	@Autowired
	private MessageMapper messageMapper;
	private MessagePictureMapper messagePictureMapper;

	public Integer insert(MessageForm messageForm) {
		messageMapper.insert(messageForm);
		return messageMapper.selectId();
	}

	public void insertPicture(Integer[] name, int messageId) {
		List<MessagePictureEntity> messageEntities = new ArrayList<MessagePictureEntity>();
		messageEntities = toEntities(name, messageId);
		messagePictureMapper.insert(messageEntities);
	}

	private List<MessagePictureEntity> toEntities(Integer[] name, int messageId){
		List<MessagePictureEntity> entities = new ArrayList<MessagePictureEntity>();
		for(int i = 0; i < name.length; i++) {
			if(name[i] != null) {
				MessagePictureEntity entity = new MessagePictureEntity();
				entity.setMessageId(messageId);
				entity.setName(String.valueOf(name[i]));
				entities.add(entity);
			}
		}
		return entities;
	}

	/*public MessageDto select(int messageId) {
		return messageMapper.select(messageId);
	}

	public void update(MessageForm messageForm) {
		messageMapper.update(messageForm);
		return;
	}*/

}
