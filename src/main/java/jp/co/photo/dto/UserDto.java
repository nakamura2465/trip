package jp.co.photo.dto;

import java.util.Date;

public class UserDto {
	private int id;
	private String name;
	private String account;
	private String password;
	private String filePath;
	private Date createdDate;
	private Date updatedDate;

	public int getId() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}

	public String getAccount() {
		return this.account;
	}

	public String getPassword() {
		return this.password;
	}

	public String getFilePath() {
		return this.filePath;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}


	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setId(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}
