package jp.co.photo.dto;

import java.util.Date;

public class MessagePictureDto {

	private int id;
	private int messageId;
	private String name;
	private Date createdDate;

	public int getId() {
		return this.id;
	}

	public int getMessageId() {
		return this.messageId;
	}

	public String getName() {
		return this.name;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
}
