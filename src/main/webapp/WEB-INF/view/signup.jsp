<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
</head>
<body>
	<div class = "signup-wrapper">
		<form:form modelAttribute = "userForm">
			<label for="account">account</label><br>
		    <form:input path="account"/><br>
		    <label for ="name">名前</label><br>
		    <form:input path="name"/><br>
		    <label for="password">password</label><br>
		    <form:input type = "password" path="password" /><br>
		    <input type="submit" value = "新規登録">
		</form:form>
	</div>
</body>
</html>