<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
</head>
<body>
	<div class = "login-wrapper">
		<form:form modelAttribute = "userForm">
			<label for="account">account</label><br>
		    <form:input path="account"/><br>
		    <label for="password">password</label><br>
		    <form:input type = "password" path="password" />
		    <input type="submit" value = "ログイン">
		</form:form>
	</div>
</body>
</html>