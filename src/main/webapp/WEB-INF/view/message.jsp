<!DOCTYPE html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
<meta charset="utf-8">
<title>新規投稿</title>
</head>
<body>
	<h1>${message}</h1>
	<form:form modelAttribute="messageForm">
		<form:input type="hidden" path="userId" value="1" />
		<label for="title">件名</label>
		<form:input path="title" /><br />

		<label for="categoryId">カテゴリ</label>
		<form:input path="categoryId" /><br />

		<label for="content">投稿本文</label>
		<form:input path="content" /><br />

		<!-- <input type="file" accept="image/*" name="picture0"/><br />
		<input type="file" accept="image/*" name="picture1"/><br />
		<input type="file" accept="image/*" name="picture2"/><br />
		<input type="file" accept="image/*" name="picture3"/><br /> -->
		<input type="submit" value="投稿">

	</form:form>
</body>
</html>