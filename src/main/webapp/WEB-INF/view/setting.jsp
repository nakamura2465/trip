<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
	<head>
		<meta charset="utf-8">
		<title>ユーザー編集</title>
		<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
	</head>
	<body>
		<header>
			<div class = "loginUser">
				<c:if test = "${loginUser != null}">
					<p>ようこそ<c:out value ="${loginUser.name}"/>さん</p>
				</c:if>
			</div>
		</header>
		<div class = "main">
			<div class = "setting-wrapper">
				<form:form modelAttribute = "userForm"  >
					<form:input path="id" id="id" value = "${userForm.id }" type ="hidden"/><br>
					<label for="account">account</label><br>
					<form:input path="account" value = "${userForm.account }" /><br>
					<label for="name">名前</label><br>
					<form:input path="name" value = "${userForm.name }" /><br>
					<label for="password">password</label><br>
					<form:input path="password" type="password"/><br>
					<input type="submit" value="投稿"/>
 				</form:form>
			</div>
		</div>
	</body>
	<script src="<c:url value="/resources/js/application.js" />"></script>
</html>
