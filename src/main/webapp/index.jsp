<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>
	<head>
		<meta charset="utf-8">
		<title>Welcome</title>
	</head>
	<body>
		<header>
			<div class = "loginUser">
				<c:if test = "${loginUser != null}">
					<p>ようこそ<c:out value ="${loginUser.name}"/>さん</p>
				</c:if>
			</div>
		</header>
		<c:url value="/showMessage.html" var="messageUrl" />
	</body>
</html>